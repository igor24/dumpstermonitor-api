package udp.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Игорь Золотницкий
 */
public class UDPServer implements Runnable {
  private final ExecutorService executeService = Executors.newFixedThreadPool(5 * Runtime.getRuntime().availableProcessors());
  static private final Logger LOGGER = LoggerFactory.getLogger(UDPServer.class);
  private final DatagramSocket ds;

  private final InetAddress udpHost;
  private final int udpPort;
  private final int udpBuffLen;


  public UDPServer(final String udpHost, final int udpPort, final int udpBuffLen) throws UnknownHostException, SocketException {
    this.udpHost = (udpHost == null || udpHost.isEmpty()) ? null : InetAddress.getByName(udpHost);
    this.udpPort = udpPort;
    this.udpBuffLen = udpBuffLen;
    ds = new DatagramSocket(udpPort, this.udpHost);
    LOGGER.info("Прослушивается UDP {}:{}, длинна буфера {}", (this.udpHost == null) ? "*" : this.udpHost.toString(), udpPort, udpBuffLen);
  }

  public void run() {
    while (true) {
      try {
        DatagramPacket pack = new DatagramPacket(new byte[udpBuffLen], udpBuffLen);
        ds.receive(pack);
        this.executeService.execute(new ServerThread(pack));
      } catch (Exception err) {
        LOGGER.error(err.getMessage());
      }
    }
  }

  public InetAddress getHost() throws UnknownHostException {
    return udpHost;
  }

  public int getPort() {
    return udpPort;
  }

  public int getBuffLen() {
    return udpBuffLen;
  }
}
