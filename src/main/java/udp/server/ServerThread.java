package udp.server;

import dumpster.monitor.api.utils.ApplicationContextManager;
import dumpster.monitor.api.events.ReceiveDumpsterEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.net.DatagramPacket;

/**
 * @author Игорь Золотницкий
 */
public class ServerThread extends Thread {
    static private final Logger LOGGER = LoggerFactory.getLogger(ServerThread.class);
    private final DatagramPacket packet;

    public ServerThread(DatagramPacket packet) {
        this.packet = packet;
    }

    public void run () {
        byte[] data = new byte[packet.getLength()];
        System.arraycopy(packet.getData(), packet.getOffset(), data, 0, packet.getLength());
        final String message = new String(data);
        LOGGER.info(String.format("Получено сообщение: %s", message));
        ApplicationContextManager.getAppContext().publishEvent(new ReceiveDumpsterEvent(message));
        //bus.post(message).now();
    }
}
