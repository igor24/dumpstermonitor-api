package dumpster.monitor.api.resources;

import dumpster.monitor.api.exceptions.ApiException;
import dumpster.monitor.api.model.Dumpster;
import dumpster.monitor.api.services.DumpsterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/dumpsters")
@Produces(MediaType.APPLICATION_JSON)
public final class DumpsterResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(DumpsterResource.class);
    private final DumpsterService dumpsterService;

    @Inject
    public DumpsterResource(final DumpsterService dumpsterService) {
        this.dumpsterService = dumpsterService;
    }

    @GET
    public List<Dumpster> getDumpsters() throws ApiException {
        LOGGER.info("Запрос списка мусорных баков");
        return dumpsterService.getDumpsters();
    }

    @POST
    public Response createDumpster(final Dumpster dumpster) throws ApiException {
        LOGGER.info("Создание нового бака {}", dumpster);
        final Dumpster createdDumpster = dumpsterService.addDumpster(dumpster);
        return Response.status(Response.Status.CREATED)
                .entity(createdDumpster)
                .build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteDumpster(
            @PathParam("id")
            final int partNumber
    ) throws ApiException {
        LOGGER.info("Удаление бака № ", partNumber);
        dumpsterService.removeDumpster(partNumber);
        return Response.noContent().build();
    }
}
