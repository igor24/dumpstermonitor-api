package dumpster.monitor.api.resources;

import dumpster.monitor.api.exceptions.ApiException;
import dumpster.monitor.api.model.Options;
import dumpster.monitor.api.services.OptionsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/options")
@Produces(MediaType.APPLICATION_JSON)
public final class OptionsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(OptionsResource.class);
    private final OptionsService optionsService;

    @Inject
    public OptionsResource(final OptionsService optionsService) {
        this.optionsService = optionsService;
    }

    @GET
    public Options getOptions() throws ApiException {
        LOGGER.info("Запрос настроек");
        return optionsService.getOptions(Options.MAIN_OPTIONS);
    }

    @PUT
    public Options updateOptions(final Options options) throws ApiException {
        LOGGER.info("Обновление настроек {}", options);
        return optionsService.updateOptions(options);
    }
}
