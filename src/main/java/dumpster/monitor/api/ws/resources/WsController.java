package dumpster.monitor.api.ws.resources;

import dumpster.monitor.api.model.Dumpster;
import dumpster.monitor.api.model.Options;
import dumpster.monitor.api.services.DumpsterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import java.util.List;

@Controller
public class WsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WsController.class);
    private final DumpsterService dumpsterService;

    @Autowired
    private SimpMessagingTemplate template;

    @Inject
    public WsController(DumpsterService dumpsterService) {
        this.dumpsterService = dumpsterService;
    }

    @MessageMapping("/connect")
    @SendTo("/dumpsters")
    public List<Dumpster> sendDumpsterData() throws Exception {
        LOGGER.info("Запрос списка баков и настроек по ws");
        return dumpsterService.getDumpsters();
    }

    public void createDumpster(Dumpster dumpster) {
        LOGGER.info("[createDumpster] Отправка данных по ws {}", dumpster);
        this.template.convertAndSend("/dumpsters/create", dumpster);
    }

    public void updateDumpster(Dumpster dumpster) {
        LOGGER.info("[updateDumpster] Отправка данных по ws {}", dumpster);
        this.template.convertAndSend("/dumpsters/update", dumpster);
    }

    public void deleteDumpster(int partNumber) {
        LOGGER.info("[deleteDumpster] Отправка данных по ws {}", partNumber);
        this.template.convertAndSend("/dumpsters/delete", partNumber);
    }

    public void updateOptions(Options options) {
        LOGGER.info("[updateOptions] Отправка данных по ws {}", options);
        this.template.convertAndSend("/options", options);
    }
}
