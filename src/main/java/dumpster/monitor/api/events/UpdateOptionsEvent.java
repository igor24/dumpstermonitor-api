package dumpster.monitor.api.events;

import dumpster.monitor.api.model.Options;
import org.springframework.context.ApplicationEvent;

public final class UpdateOptionsEvent extends ApplicationEvent {
    private static final long serialVersionUID = -5393744896435521299L;
    private transient final Options options;

    public UpdateOptionsEvent (final Options options) {
        super(options);
        this.options = options;
    }

    public Options getOptions() {
        return options;
    }

    @Override
    public String toString() {
        return String.format("UpdateOptionsEvent[options=%s]", options);
    }
}
