package dumpster.monitor.api.events;

import dumpster.monitor.api.model.Dumpster;
import org.springframework.context.ApplicationEvent;

public final class UpdateDumpsterEvent extends ApplicationEvent {
    private static final long serialVersionUID = 4376547706518310011L;
    private transient final Dumpster dumpster;

    public UpdateDumpsterEvent(final Dumpster dumpster) {
        super(dumpster);
        this.dumpster = dumpster;
    }

    public Dumpster getDumpster() {
        return dumpster;
    }

    @Override
    public String toString() {
        return String.format("UpdateDumpsterEvent[dumpster=%s]", dumpster);
    }
}
