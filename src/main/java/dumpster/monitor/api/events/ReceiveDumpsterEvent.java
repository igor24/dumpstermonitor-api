package dumpster.monitor.api.events;

import org.springframework.context.ApplicationEvent;

public final class ReceiveDumpsterEvent extends ApplicationEvent {
    private static final long serialVersionUID = -2678179502968513988L;
    private transient final String message;

    public ReceiveDumpsterEvent(final String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("ReceiveDumpsterEvent[message=%s]", message);
    }
}
