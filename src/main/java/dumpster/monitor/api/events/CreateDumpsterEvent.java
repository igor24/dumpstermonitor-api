package dumpster.monitor.api.events;

import dumpster.monitor.api.model.Dumpster;
import org.springframework.context.ApplicationEvent;

public final class CreateDumpsterEvent extends ApplicationEvent {
    private static final long serialVersionUID = 6399049565371741315L;
    private transient final Dumpster dumpster;

    public CreateDumpsterEvent(final Dumpster dumpster) {
        super(dumpster);
        this.dumpster = dumpster;
    }

    public Dumpster getDumpster() {
        return dumpster;
    }

    @Override
    public String toString() {
        return String.format("CreateDumpsterEvent[dumpster=%s]", dumpster);
    }
}
