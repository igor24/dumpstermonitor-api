package dumpster.monitor.api.events.listeners;

import dumpster.monitor.api.events.ReceiveDumpsterEvent;
import dumpster.monitor.api.model.Dumpster;
import dumpster.monitor.api.services.DumpsterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public final class ReceiveDumpsterListener implements ApplicationListener<ReceiveDumpsterEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(ReceiveDumpsterListener.class);
    private final DumpsterService dumpsterService;

    @Inject
    public ReceiveDumpsterListener(final DumpsterService dumpsterService) {
        this.dumpsterService = dumpsterService;
    }

    @Override
    public void onApplicationEvent(ReceiveDumpsterEvent event) {
        LOG.info("Событие ReceiveDumpster {}", event);
        try {
            dumpsterService.updateDumpster(new Dumpster(event.getMessage()));
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }
}
