package dumpster.monitor.api.events.listeners;

import dumpster.monitor.api.events.UpdateDumpsterEvent;
import dumpster.monitor.api.ws.resources.WsController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public final class UpdateDumpsterListener implements ApplicationListener<UpdateDumpsterEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(UpdateDumpsterListener.class);
    private final WsController wsController;

    @Inject
    public UpdateDumpsterListener(final WsController wsController) {
        this.wsController = wsController;
    }

    @Override
    public void onApplicationEvent(UpdateDumpsterEvent event) {
        LOG.info("Событие UpdateDumpster {}", event);
        try {
            wsController.updateDumpster(event.getDumpster());
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }
}
