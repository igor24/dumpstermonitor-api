package dumpster.monitor.api.repository.jpa;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface Java8Repository<T, ID extends Serializable> extends Repository<T, ID> {
  Optional<T> findOne(ID id);

  List<T> findAll();

  <S extends T> S save(S entity);
}
