package dumpster.monitor.api.services;

import dumpster.monitor.api.exceptions.ApiException;
import dumpster.monitor.api.model.Dumpster;

import java.util.List;

/**
 * @author Игорь Золотинцкий
 */
public interface DumpsterService {
    /**
     * Добавить бак
     *
     * @param dumpster бак для добавления
     * @return добавленный бак
     */
    Dumpster addDumpster(Dumpster dumpster);

    /**
     * Удалить бак
     *
     * @param partNumber id бака для удаления
     * @return void
     */
    Dumpster removeDumpster(int partNumber);

    /**
     * Обновить бак
     *
     * @param dumpster бак для обновления
     * @return обновленный бак
     */
    Dumpster updateDumpster(Dumpster dumpster);

    /**
     * Получить список баков
     *
     * @return список баков
     * @throws ApiException если произошла ошибка
     */
    List<Dumpster> getDumpsters() throws ApiException;
}
