package dumpster.monitor.api.services.impl;

import dumpster.monitor.api.events.UpdateOptionsEvent;
import dumpster.monitor.api.exceptions.ApiException;
import dumpster.monitor.api.model.Options;
import dumpster.monitor.api.repository.jpa.OptionsRepository;
import dumpster.monitor.api.services.OptionsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

/**
 * @author Игорь Золотницкий
 */
@Service
@Transactional(rollbackFor = {ApiException.class, ConstraintViolationException.class})
public class OptionsServiceImpl implements OptionsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OptionsServiceImpl.class);
    private final OptionsRepository optionsRepository;
    private final ApplicationContext applicationContext;

    @Inject
    public OptionsServiceImpl(final OptionsRepository optionsRepository, final ApplicationContext applicationContext) {
        this.optionsRepository = optionsRepository;
        this.applicationContext = applicationContext;
    }

    @Override
    @Transactional(readOnly = true)
    public Options getOptions(String id) throws ApiException {
        return optionsRepository.findById(id);
    }

    @Override
    public Options updateOptions(Options options) throws ApiException {
        final Options updatedOptions = optionsRepository.save(options);
        applicationContext.publishEvent(new UpdateOptionsEvent(updatedOptions));
        return updatedOptions;
    }
}
