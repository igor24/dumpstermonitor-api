package dumpster.monitor.api.exceptions;

public class ApiException extends RuntimeException {
    private static final long serialVersionUID = 877595375310062263L;

    /**
     * HTTP error code
     */
    private final int errorCode;

    /**
     * Constructor.
     *
     * @param errorCode the HTTP status code for this exception.
     * @param message   human readable message.
     * @param cause     reason for this exception.
     */
    public ApiException(final int errorCode, final String message, final Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * Constructor.
     *
     * @param errorCode the HTTP status code for this exception.
     * @param cause     reason for this exception.
     */
    public ApiException(final int errorCode, final Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    /**
     * Constructor.
     *
     * @param errorCode the HTTP status code for this exception.
     * @param message   human readable message.
     */
    public ApiException(final int errorCode, final String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Return the HTTP status code for this exception.
     *
     * @return the HTTP status code.
     */
    public int getErrorCode() {
        return errorCode;
    }
}
