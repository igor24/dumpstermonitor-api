package dumpster.monitor.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public final class Location {
  private final double latitude;
  private final double longitude;

  public Location(final double latitude, final double longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  @JsonCreator
  public static Location fromJson(
      @JsonProperty("latitude")
      final double latitude,
      @JsonProperty("longitude")
      final double longitude) {
    return new Location(latitude, longitude);
  }

  public double getLatitude() {
    return this.latitude;
  }

  public double getLongitude() {
    return this.longitude;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final Location location = (Location) o;
    return Objects.equals(latitude, location.latitude) && Objects.equals(longitude, location.longitude);
  }

  @Override
  public int hashCode() {
    return Objects.hash(latitude, longitude);
  }

  @Override
  public String toString() {
    return String.format("Location[latitude=%.4f, longitude=%.4f]", latitude, longitude);
  }
}
