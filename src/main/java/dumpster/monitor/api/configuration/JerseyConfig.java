package dumpster.monitor.api.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import dumpster.monitor.api.resources.DumpsterResource;
import dumpster.monitor.api.resources.OptionsResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/api/v1")
public class JerseyConfig extends ResourceConfig {
  public JerseyConfig() {
    //packages("dumpster.monitor.api.resources");
    register(DumpsterResource.class);
    register(OptionsResource.class);
  }

  @Bean
  public Jackson2ObjectMapperBuilder jacksonBuilder() {
    return new Jackson2ObjectMapperBuilder()
        .featuresToDisable(SerializationFeature.WRITE_DATES_WITH_ZONE_ID)
        .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
  }

  @Bean
  @Primary
  public ObjectMapper objectMapper(final Jackson2ObjectMapperBuilder builder) {
    return builder.build();
  }
}
