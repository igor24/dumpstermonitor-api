package dumpster.monitor.api;

import dumpster.monitor.api.utils.ApplicationContextManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import udp.server.UDPServer;

/**
 * @author Игорь Золотницкий
 */
@SpringBootApplication
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main (final String[] args) throws Exception {
        SpringApplication.run(Application.class, args);

        final UDPServer server = ApplicationContextManager.getAppContext().getBean(UDPServer.class);
        Thread serverThread = new Thread(server);
        serverThread.start();
    }
}
